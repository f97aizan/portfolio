const express = require('express')
const dotenv = require('dotenv').config()
const color = require('colors')
const logger = require('morgan')
const path = require('path')
const parser = require('body-parser')

// const __dirname = dirname(fileURLToPath(import.meta.url))
const app = express();
const port = process.env.PORT || 8080

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(express.static(path.join(__dirname, 'public')))
app.use(parser.urlencoded({extended: true}))

function mylogger(req,res,next){
    console.log(`Request Method: ${req.method}`.bgMagenta);
    console.log(`Request URL: ${req.url}`.bgMagenta);
    next()
}
// app.use(mylogger)
app.use(logger('dev'))



app.get('/home', (req,res)=>{
    
    res.render('index')   
})
app.get('/submit', (req,res)=>{
    res.render('submit', {name: req.body.name, email: req.body.email})
})


app.post('/submit', (req,res)=>{
    res.render('submit', {name: req.body.name, email: req.body.email})
})



app.listen(port | 8080, ()=>{
    console.log(`Server start spinning at http://localhost:${port}`.green.bgCyan)
})